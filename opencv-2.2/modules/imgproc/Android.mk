LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_imgproc

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

LOCAL_SRC_FILES :=   src/accum.cpp src/approx.cpp src/canny.cpp src/color.cpp src/contours.cpp src/convhull.cpp src/corner.cpp src/cornersubpix.cpp src/deriv.cpp src/distransform.cpp src/emd.cpp src/featureselect.cpp src/featuretree.cpp src/filter.cpp src/floodfill.cpp src/geometry.cpp src/grabcut.cpp src/histogram.cpp src/hough.cpp src/imgwarp.cpp src/inpaint.cpp src/kdtree.cpp src/linefit.cpp src/lsh.cpp src/matchcontours.cpp src/moments.cpp src/morph.cpp src/precomp.cpp src/pyramids.cpp src/pyrsegmentation.cpp src/rotcalipers.cpp src/samplers.cpp src/segmentation.cpp src/shapedescr.cpp src/smooth.cpp src/spilltree.cpp src/subdivision2d.cpp src/sumpixels.cpp src/tables.cpp src/templmatch.cpp src/thresh.cpp src/undistort.cpp src/utils.cpp

LOCAL_CFLAGS := 

LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/src  $(OPENCV_INCLUDES) $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
