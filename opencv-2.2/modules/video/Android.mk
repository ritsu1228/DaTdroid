LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_video

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

LOCAL_SRC_FILES :=   src/bgfg_acmmm2003.cpp src/bgfg_codebook.cpp src/bgfg_common.cpp src/bgfg_gaussmix.cpp src/camshift.cpp src/kalman.cpp src/lkpyramid.cpp src/motempl.cpp src/optflowbm.cpp src/optflowgf.cpp src/optflowhs.cpp src/optflowlk.cpp src/precomp.cpp

LOCAL_CFLAGS := 

LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/src  $(OPENCV_INCLUDES) $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
