LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_highgui

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

LOCAL_SRC_FILES :=   src/bitstrm.cpp src/grfmt_base.cpp src/grfmt_bmp.cpp src/grfmt_jpeg.cpp src/grfmt_png.cpp src/grfmt_tiff.cpp src/loadsave.cpp src/precomp.cpp src/utils.cpp src/grfmt_sunras.cpp src/grfmt_pxm.cpp src/window.cpp

LOCAL_CFLAGS := 

LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/src  $(OPENCV_INCLUDES) $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
