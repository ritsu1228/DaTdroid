LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_objdetect

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

LOCAL_SRC_FILES :=   src/cascadedetect.cpp src/distancetransform.cpp src/featurepyramid.cpp src/fft.cpp src/haar.cpp src/hog.cpp src/lsvmparser.cpp src/matching.cpp src/precomp.cpp src/resizeimg.cpp src/routine.cpp

LOCAL_CFLAGS := 

LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/src  $(OPENCV_INCLUDES) $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
