LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_core

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

LOCAL_SRC_FILES :=   src/alloc.cpp src/arithm.cpp src/array.cpp src/convert.cpp src/copy.cpp src/datastructs.cpp src/drawing.cpp src/dxt.cpp src/lapack.cpp src/mathfuncs.cpp src/matmul.cpp src/matop.cpp src/matrix.cpp src/out.cpp src/persistence.cpp src/precomp.cpp src/rand.cpp src/stat.cpp src/system.cpp src/tables.cpp

LOCAL_CFLAGS := 

LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/src  $(OPENCV_INCLUDES) $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
