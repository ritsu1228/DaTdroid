APP_BUILD_SCRIPT := $(call my-dir)/Android.mk
APP_PROJECT_PATH := $(call my-dir)

APP_STL := gnustl_static

# The ARMv7 is significanly faster due to the use of the hardware FPU
APP_ABI := armeabi-v7a

APP_MODULES := png jpeg jasper zlib opencv_lapack opencv_core opencv_imgproc opencv_highgui opencv_objdetect opencv_video

APP_CPPFLAGS := -frtti -fexceptions -mfpu=vfpv3-d16 -ftree-vectorize -mfloat-abi=softfp -ffast-math