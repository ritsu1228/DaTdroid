#you may override this if you move the build
#just define it before including this or on the command line - or with
#an environment variable
#this points to the root of the opencv trunk - where the original opencv 
#sources are - with modules 3rparty ...
OPENCV_ROOT := $(call my-dir)

#you may override this same as above
#this points to the actually directory that you built opencv for android from
#maybe in under opencv/android/build
OPENCV_BUILD_ROOT := $(OPENCV_ROOT)

OPENCV_INCLUDES := $(OPENCV_ROOT)/modules/core/include \
		   $(OPENCV_ROOT)/modules/imgproc/include \
		   $(OPENCV_ROOT)/modules/objdetect/include \
		   $(OPENCV_ROOT)/modules/highgui/include \
		   $(OPENCV_ROOT)/modules/video/include \
		   $(OPENCV_ROOT)/3rdparty/include \
		   $(OPENCV_BUILD_ROOT)/include \
		   $(OPENCV_ROOT)/include

ARMOBJS := local/armeabi
ARMOBJS_V7A := local/armeabi-v7a

OPENCV_LIB_DIRS := -L$(OPENCV_BUILD_ROOT)/obj/$(ARMOBJS_V7A) \
    -L$(OPENCV_BUILD_ROOT)/obj/$(ARMOBJS) -L$(OPENCV_BUILD_ROOT)/bin/ndk/$(ARMOBJS) \
    -L$(OPENCV_BUILD_ROOT)/bin/ndk/$(ARMOBJS_V7A)

#order of linking very important ---- may have stuff out of order here, but
#important that modules that are more dependent come first...

OPENCV_LIBS := $(OPENCV_LIB_DIRS) -lopencv_objdetect -lopencv_imgproc \
       -lopencv_video -lopencv_highgui -lopencv_core -lopencv_lapack \
    -lzlib -lpng -ljpeg -ljasper
