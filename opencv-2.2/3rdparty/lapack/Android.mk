LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_lapack

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

LOCAL_SRC_FILES :=   dasum.c daxpy.c dbdsdc.c dbdsqr.c dcopy.c ddot.c dgebd2.c dgebrd.c dgelq2.c dgelqf.c dgels.c dgelsd.c dgemm.c dgemv_custom.c dgeqr2.c dgeqrf.c dger_custom.c dgesdd.c dgesv.c dgetf2.c dgetrf.c dgetri.c dgetrs.c dlabad.c dlabrd.c dlacpy.c dlae2.c dlaebz.c dlaed0.c dlaed1.c dlaed2.c dlaed3.c dlaed4.c dlaed5.c dlaed6.c dlaed7.c dlaed8.c dlaed9.c dlaeda.c dlaev2.c dlagtf.c dlagts.c dlaisnan.c dlals0.c dlalsa.c dlalsd.c dlamch_custom.c dlamrg.c dlaneg.c dlange.c dlanst.c dlansy.c dlapy2.c dlar1v.c dlarf.c dlarfb.c dlarfg.c dlarfp.c dlarft.c dlarnv.c dlarra.c dlarrb.c dlarrc.c dlarrd.c dlarre.c dlarrf.c dlarrj.c dlarrk.c dlarrr.c dlarrv.c dlartg_custom.c dlaruv.c dlas2.c dlascl.c dlasd0.c dlasd1.c dlasd2.c dlasd3.c dlasd4.c dlasd5.c dlasd6.c dlasd7.c dlasd8.c dlasda.c dlasdq.c dlasdt.c dlaset.c dlasq1.c dlasq2.c dlasq3.c dlasq4.c dlasq5.c dlasq6.c dlasrt.c dlasr_custom.c dlassq.c dlasv2.c dlaswp.c dlasyf.c dlatrd.c dlauu2.c dlauum.c dnrm2.c dorg2r.c dorgbr.c dorgl2.c dorglq.c dorgqr.c dorm2l.c dorm2r.c dormbr.c dorml2.c dormlq.c dormql.c dormqr.c dormtr.c dpotf2.c dpotrf.c dpotri.c dpotrs.c drot.c dscal.c dstebz.c dstein.c dstemr.c dsteqr.c dsterf.c dswap.c dsyevr.c dsymv.c dsyr.c dsyr2.c dsyr2k.c dsyrk.c dsytd2.c dsytf2.c dsytrd.c dsytrf.c dsytri.c dsytrs.c dtrmm.c dtrmv.c dtrsm.c dtrti2.c dtrtri.c dtrtrs.c f77_aloc.c idamax.c ieeeck.c iladlc.c iladlr.c ilaenv_custom.c ilaslc.c ilaslr.c iparmq.c isamax.c pow_di.c pow_ii.c pow_ri.c precomp.c sasum.c saxpy.c sbdsdc.c sbdsqr.c scopy.c sdot.c sgebd2.c sgebrd.c sgelq2.c sgelqf.c sgels.c sgelsd.c sgemm.c sgemv_custom.c sgeqr2.c sgeqrf.c sger_custom.c sgesdd.c sgesv.c sgetf2.c sgetrf.c sgetri.c sgetrs.c slabad.c slabrd.c slacpy.c slae2.c slaebz.c slaed0.c slaed1.c slaed2.c slaed3.c slaed4.c slaed5.c slaed6.c slaed7.c slaed8.c slaed9.c slaeda.c slaev2.c slagtf.c slagts.c slaisnan.c slals0.c slalsa.c slalsd.c slamch_custom.c slamrg.c slaneg.c slange.c slanst.c slansy.c slapy2.c slar1v.c slarf.c slarfb.c slarfg.c slarfp.c slarft.c slarnv.c slarra.c slarrb.c slarrc.c slarrd.c slarre.c slarrf.c slarrj.c slarrk.c slarrr.c slarrv.c slartg_custom.c slaruv.c slas2.c slascl.c slasd0.c slasd1.c slasd2.c slasd3.c slasd4.c slasd5.c slasd6.c slasd7.c slasd8.c slasda.c slasdq.c slasdt.c slaset.c slasq1.c slasq2.c slasq3.c slasq4.c slasq5.c slasq6.c slasrt.c slasr_custom.c slassq.c slasv2.c slaswp.c slatrd.c slauu2.c slauum.c snrm2.c sorg2r.c sorgbr.c sorgl2.c sorglq.c sorgqr.c sorm2l.c sorm2r.c sormbr.c sorml2.c sormlq.c sormql.c sormqr.c sormtr.c spotf2.c spotrf.c spotri.c spotrs.c srot.c sscal.c sstebz.c sstein.c sstemr.c ssteqr.c ssterf.c sswap.c ssyevr.c ssymv.c ssyr2.c ssyr2k.c ssyrk.c ssytd2.c ssytrd.c strmm.c strmv.c strsm.c strti2.c strtri.c strtrs.c s_cat.c s_cmp.c s_copy.c xerbla.c

LOCAL_CFLAGS := 

LOCAL_C_INCLUDES :=  $(LOCAL_PATH)/../include $(LOCAL_PATH)

include $(BUILD_STATIC_LIBRARY)
