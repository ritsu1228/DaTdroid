#include <jni.h>
#include "common.h"
#include "Pipeline.h"

extern "C" {
JNIEXPORT void Java_ritsu_datdroid_DaTdroidView_Track(JNIEnv* env,
													  jobject thiz,
													  jint width,
													  jint height,
													  jbyteArray yuv,
													  jintArray rgba)
{
	jbyte* pyuv = env->GetByteArrayElements(yuv, 0);
	jint* prgba = env->GetIntArrayElements(rgba, 0);

	cv::Mat myuv(height + height/2, width, CV_8UC1, pyuv);
	cv::Mat mrgba(height, width, CV_8UC4, prgba);
	cvtColor(myuv, mrgba, CV_YUV420sp2BGR, 4);

	cv::Mat mrgb(height, width, CV_8UC3);
	cvtColor(mrgba, mrgb, CV_BGRA2RGB, 3);

	Pipeline* pEng = Pipeline::getInstance();
	pEng->ProcessFrame(mrgb, width, height);
	cvtColor(mrgb, mrgba, CV_BGR2RGBA, 4);

	env->ReleaseIntArrayElements(rgba, prgba, 0);
	env->ReleaseByteArrayElements(yuv, pyuv, 0);
}

}
