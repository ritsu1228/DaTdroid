#ifndef PIPELINE_H
#define PIPELINE_H

#include "common.h"
#include "Utils.h"
#include "Filters.h"
#include "Trackers.h"
#include "ObjDetect.h"

/*******************************************************************************
 * 
 * 
 * 
 *******************************************************************************/
class Pipeline 
{
public:
	~Pipeline();

private:
	Pipeline();
	Pipeline(const Pipeline&);
	
public:
	void Reset();
	void ProcessFrame(cv::Mat& matFrame, int width, int height);
	static Pipeline* getInstance();

public:
	static void SwapRGB(boost::uint8_t* p, int width, int height)
	{
		for(int i = 0; i < width*height; i ++)
		{
			boost::uint8_t c = p[3 * i];
			p[3 * i] = p[3 * i + 2];
			p[3 * i + 2] = c;
		}
	}

private:
	ObjectDetector _Detector;
	std::vector<cv::Rect> _ObjRect;
	std::vector<cv::Rect> _ObjRectFiltered;
	TrackerManager _TM;
	float _Fps;
	boost::int32_t _DetectorActCount;
	std::vector<boost::int32_t> _DetectionPosition;
	bool _RealtimeDisplay;
	boost::uint32_t _FrmCnt;

	static Pipeline* _Inst;
};

#endif // PIPELINE_H
