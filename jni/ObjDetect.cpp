#include "ObjDetect.h"
#include "Logger.h"

/******************************************************************************
 *
 * ObjectDetector
 *
 ******************************************************************************/
ObjectDetector::ObjectDetector()
{

}

/**
 *
 */
ObjectDetector::~ObjectDetector()
{

}

/**
 *
 */
void ObjectDetector::Init()
{
	_Hog.setSVMDetector(cv::HOGDescriptor::getDefaultPeopleDetector());
	LOG4CPLUS_INFO("ObjectDetector::Init(): selected default people detector.");
}

/**
 *
 */
void ObjectDetector::Run(cv::Mat& frame, std::vector<cv::Rect>& found, std::vector<cv::Rect>& filtered)
{
	LOG4CPLUS_INFO("ObjectDetector::Run(): starting detection...\n");

	_Hog.detectMultiScale(frame, found, 0, cv::Size(8,8), cv::Size(32,32));

	size_t i, j;
	for(i = 0; i < found.size(); i ++)
	{
		cv::Rect r = found[i];
		for(j = 0; j < found.size(); j ++)
			if( j != i && (r & found[j]) == r)
				break;
		if( j == found.size() )
			filtered.push_back(r);
	}

	for( size_t i = 0; i < filtered.size(); i ++ )
	{
		cv::Rect r = filtered[i];
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);
		cv::rectangle(frame, r.tl(), r.br(), cv::Scalar(0,255,0), 1);
	}

	LOG4CPLUS_INFO("ObjectDetector::Run(): found %d, filtered %d.\n", found.size(), filtered.size());
}
