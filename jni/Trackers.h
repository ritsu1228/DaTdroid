#ifndef __TRACKERS_H__
#define __TRACKERS_H__

#include <vector>
#include "common.h"
#include "Filters.h"

/*******************************************************************************
 * 
 * Base class for Tracker.
 * 
 *******************************************************************************/
class TrackerBase
{
public:
	TrackerBase() : _id(-1) {}
	virtual ~TrackerBase() {}
	
protected:
	TrackerBase(const TrackerBase& rhs);

public:
	void SetID(boost::int32_t id) {_id = id;}
	
public:
	virtual void Run(const image_t& frame) = 0;
	virtual void DrawParticles(image_t& frame) = 0;
	virtual bool NeedCorrection() = 0;
	virtual void CorrectTracker(const image_t& frame, const std::vector<cv::Rect>& roilist) = 0;

protected:
	boost::int32_t _id;
//	log4cplus::Logger _Logger;
};

/*******************************************************************************
 * 
 * Tracker using color feature.
 * 
 *******************************************************************************/
class Tracker_Color : public TrackerBase
{
public:
	enum CONSTS
	{
		RESAMPLE_CNT_THRESHOLD = 30
	};
	
public:
	Tracker_Color();
	virtual ~Tracker_Color();
	
protected:
	Tracker_Color(const Tracker_Color& rhs);
	
public:
	virtual void Init(const ColorHistogram_Divided& hist, boost::tuple<int, int> initpos, boost::tuple<int, int> size);
	virtual void Run(const image_t& frame);
	virtual void DrawParticles(image_t& frame);
	virtual bool NeedCorrection();
	virtual void CorrectTracker(const image_t& frame, const std::vector<cv::Rect>& roilist) { _PF.CorrectFilter(frame, roilist); }
	
protected:
	ParticleFilter_Color_2 _PF;
	boost::int32_t _ResamplingThreshold;
	bool _DetectEveryFrame;
	bool _UseRateThreshold;
	double _ResamplingRateThreshold;
};

/*******************************************************************************
 * 
 * Tracker using kalman filter.
 * 
 *******************************************************************************/
class Tracker_Kalman : public TrackerBase
{
public:
	Tracker_Kalman();
	~Tracker_Kalman();

protected:
	Tracker_Kalman(const Tracker_Kalman&);
};

/*******************************************************************************
 * 
 * Tracker manager.
 * 
 *******************************************************************************/
class TrackerManager
{
public:
	typedef std::vector<TrackerBase*>::iterator tracker_iter_t;
	
public:
	TrackerManager();
	virtual ~TrackerManager();
	
protected:
	TrackerManager(const TrackerManager& rhs);
	
public:
	virtual void Init();
	virtual void Cleanup();
	virtual void Run(image_t& frame, const std::vector<cv::Rect>& roilist, boost::int32_t frmcnt);
	virtual void Update(image_t& frame, const std::vector<cv::Rect>& roilist);
	virtual bool NeedCorrection() { return _TrackersNeedCorrection; }
	size_t GetTrackerNumber() const { return _TrackerList.size(); }
	
protected:
//	log4cplus::Logger _Logger;
	boost::int32_t _CurrentFrameNum;
	std::vector<TrackerBase*> _TrackerList;
	bool _TrackersNeedCorrection;
	std::vector<size_t> _TrackerIdxForCorr;
};

#endif
