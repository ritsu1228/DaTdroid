#ifndef __FILTERS_H__
#define __FILTERS_H__

#include <vector>
#include <boost/tuple/tuple.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/normal_distribution.hpp>
#include "common.h"
#include "Utils.h"

/*******************************************************************************
 * 
 * Base class for Bayesian filters.
 * 
 *******************************************************************************/
class BayesianFilter
{
public:
	BayesianFilter() {}
	virtual ~BayesianFilter() {}
	
public:
	virtual void Run(const image_t& frame) = 0;
	boost::tuple<int, int> GetExpectedPos() { return _Pos; }
	virtual const boost::tuple<int, int> GetExpectedPos() const { return _Pos; };
	virtual void DrawParticles(image_t& frame);
	
protected:
	boost::tuple<int, int> _Pos;
	boost::tuple<int, int> _Size;
};
 
/*******************************************************************************
 * 
 * Base class for Particle filters.
 * 
 *******************************************************************************/
class ParticleFilter : public BayesianFilter
{
public:
	typedef float weight_t;
	
	struct particle_t
	{
		particle_t()
			: _x(0),
			  _y(0),
			  _weight(0)
		{
			
		}
		
		particle_t(int x, int y, float weight)
			: _x(x),
			  _y(y),
			  _weight(weight)
		{
			
		}
		
		int _x;
		int _y;
		weight_t _weight;
	};
	
public:
	ParticleFilter() {}
	virtual ~ParticleFilter() {}
	
public:
	virtual void Run(const image_t& frame) = 0;
	std::vector<particle_t>& GetParticles() { return _Particles; }
	const std::vector<particle_t>& GetParticles() const { return _Particles; }
	
protected:
	std::vector<particle_t> _Particles;
};

/*******************************************************************************
 * 
 * Particle filter based on color.
 * 
 *******************************************************************************/
class ParticleFilter_Color : public ParticleFilter
{
public:
	ParticleFilter_Color();
	virtual ~ParticleFilter_Color();
	
public:
	virtual void Init(const ColorHistogram& hist, boost::tuple<int, int> initpos, boost::tuple<int, int> size);
	virtual void Run(const image_t& frame);
	virtual void CorrectFilter(const image_t& frame, const std::vector<cv::Rect>& roilist) {frame.isContinuous(); roilist.size();}
	boost::tuple<int, int> GetRefSize() { return _Size; }
	ParticleFilter::weight_t GetEffectiveSampleSize() {return _Neff;}
	void ResetResampleCounter() {_ResampleCnt = 0; _ResampleDuration = 0; _ResampleRateCount = 0; _ResampleRate = 1.0e+100;}
	boost::int32_t GetResampleCounter() const { return _ResampleCnt; }
	double GetResampleRate() const { return _ResampleRate; }
	
	
private:
	void Resample();
	void Predict();
	void Weight(const image_t& frame);
	void Measure();
	void CalcEffectiveSampleSize();
	virtual ParticleFilter::weight_t Likelihood(const image_t& frame, const ParticleFilter::particle_t& p);
	
protected:
	boost::random::mt19937 _RandEng;
	boost::random::uniform_01<float> _Uniform;
	boost::random::normal_distribution<float> _Gaussian;

	boost::int32_t _Np;
	ColorHistogram _RefHist;
	ParticleFilter::weight_t _Neff;
	ParticleFilter::weight_t _Threshold;
	ParticleFilter::weight_t _MaxWeight;
	ParticleFilter::weight_t _MinWeight;
	boost::uint32_t _ZeroWeightParticles;
	float _ZeroWeightPercent;
	boost::int32_t _ResampleCnt;
	boost::int32_t _ReplacedSample;
	boost::int32_t _ResampleDuration; /** frame counts for 10 resamplings. */
	boost::int32_t _ResampleRateCount; /** resample counts for resample rate. */
	float _ResampleRate; /** measured in frames/resampling. */
	boost::int32_t _ResampleRateStatRange; /** for how many resamplings calculate the resampling rate. */
};

/*******************************************************************************
 * 
 * Particle filter based on color.
 * 
 *******************************************************************************/
class ParticleFilter_Color_2 : public ParticleFilter_Color
{
public:
	struct roi_candidate
	{
		roi_candidate()
			: _rect(),
			  _w(0.0f)
		{
			
		}
		
		roi_candidate(const cv::Rect& rect, ParticleFilter::weight_t w)
			: _rect(rect),
			  _w(w)
		{
			
		}
		
		cv::Rect _rect;
		ParticleFilter::weight_t _w;
	};
	
public:
	ParticleFilter_Color_2();
	virtual ~ParticleFilter_Color_2();

public:
	virtual void Init(const ColorHistogram_Divided& hist, boost::tuple<int, int> initpos, boost::tuple<int, int> size);
	virtual void CorrectFilter(const image_t& frame, const std::vector<cv::Rect>& roilist);
	
private:
	virtual ParticleFilter::weight_t Likelihood(const image_t& frame, const ParticleFilter::particle_t& p);
	
private:
	ColorHistogram_Divided _RefHist_2;
	ParticleFilter::weight_t _ColorHistThreshold;
};

/*******************************************************************************
 * 
 * Kalman filter.
 * 
 *******************************************************************************/
class KalmanFilter : public BayesianFilter
{
public:
	KalmanFilter();
	virtual ~KalmanFilter();

public:
	virtual void Init(boost::tuple<int, int> initpos, boost::tuple<int, int> size);
	virtual void Run(const image_t& frame);
	
protected:
	cv::KalmanFilter _kf;
	float _dt;
};

#endif
