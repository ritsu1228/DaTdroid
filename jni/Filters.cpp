#include <boost/bind.hpp>
#include <ctime>
#include "Filters.h"
#include "Utils.h"
#include "Logger.h"

/*******************************************************************************
 * 
 * Base class for Bayesian filters.
 * 
 *******************************************************************************/
void BayesianFilter::DrawParticles(image_t& frame)
{
	cv::Rect r(_Pos.get<0>(), _Pos.get<1>(), _Size.get<0>(), _Size.get<1>());
	r.x += cvRound(r.width*0.1);
	r.width = cvRound(r.width*0.8);
	r.y += cvRound(r.height*0.07);
	r.height = cvRound(r.height*0.8);
	cv::rectangle(frame, r.tl(), r.br(), cv::Scalar(0,0,255), 1);
}

/*******************************************************************************
 * 
 * Paricle filter for Color.
 * 
 *******************************************************************************/
ParticleFilter_Color::ParticleFilter_Color()
	: ParticleFilter(),
	  _RandEng(std::time(0)),
	  _Np(50),
	  _Neff(0.0),
	  _Threshold(0.9),
	  _MaxWeight(0.0),
	  _MinWeight(1.0),
	  _ZeroWeightParticles(0),
	  _ResampleCnt(0),
	  _ReplacedSample(0),
	  _ResampleDuration(0),
	  _ResampleRateCount(0),
	  _ResampleRate(1.0e+100)
{
	_ResampleRateStatRange = 5;
}

/**
 * 
 */
ParticleFilter_Color::~ParticleFilter_Color()
{
	
}

/**
 * 
 */
void ParticleFilter_Color::Init(const ColorHistogram& hist, boost::tuple<int, int> initpos, boost::tuple<int, int> size)
{
	_RefHist = hist;
	_Size = size;
	_Particles.resize(_Np);
	
	for(std::vector<particle_t>::iterator i = _Particles.begin(); i != _Particles.end(); i ++)
	{
		i->_x = initpos.get<0>();
		i->_y = initpos.get<1>();
		i->_weight = 1.0f;
	}
}

/**
 * 
 */
ParticleFilter::weight_t ParticleFilter_Color::Likelihood(const image_t& frame, const ParticleFilter::particle_t& p)
{
	int w = _Size.get<0>();
	int h = _Size.get<1>();
	int x = p._x;
	int y = p._y;
	
//	if(x < 0) x = 0; else if(x > 320) x = frame.size().width - w;
//	if(y < 0) y = 0; else if(y > 240) y = frame.size().height - h;
//	if(x + w > frame.size().width) w = frame.size().width - x;
//	if(y + h > frame.size().height) h = frame.size().height - y;
	
	if(x < 0 || y < 0 || x > frame.size().width || y > frame.size().height)
		return 0.0;
	
	if(x + w > frame.size().width)
		w = frame.size().width - x;
	if(y + h > frame.size().height)
		h = frame.size().height - y;
	
	cv::Rect roirect(x, y, w, h);
	cv::Mat ROI = frame(roirect);
	ColorHistogram hist(ROI);
	ParticleFilter::weight_t overlap = _RefHist - hist;
	
	return 1.0 - overlap;
}

/**
 * 
 */
void ParticleFilter_Color::CalcEffectiveSampleSize()
{
	ParticleFilter::weight_t sum = 0.0;
	
	for(size_t i = 0; i < _Particles.size(); i ++)
	{
		sum += _Particles[i]._weight * _Particles[i]._weight;
	}
	
	_Neff = 1.0f/sum;
	LOG4CPLUS_INFO("ParticleFilter_Color::CalcEffectiveSampleSize(): Neff = %f, ResampleCnt = %d, ReampleRate = %f",
				   _Neff, _ResampleCnt, _ResampleRate);
}

/**
 * 
 */
void ParticleFilter_Color::Run(const image_t& frame)
{
	Predict();
	Weight(frame);
	Measure();
	CalcEffectiveSampleSize();
	
	/* frame counter, when it reaches 10, it will be reset and resample rate is calculated. */
	_ResampleDuration ++;
	
	if(_Neff < _Np * _Threshold)
	{
		Resample();
		
		_ResampleCnt ++;
		_ResampleRateCount ++;
		
		if (_ResampleRateCount >= _ResampleRateStatRange)
		{
			_ResampleRate = _ResampleDuration / static_cast<float>(_ResampleRateStatRange);
			_ResampleDuration = 0;
			_ResampleRateCount = 0;
		}
	}
}

/**
 * 
 */
void ParticleFilter_Color::Resample()
{
	std::vector<ParticleFilter::weight_t> weights(_Particles.size());
	weights[0] = _Particles[0]._weight;
	
	// 累積重み.
	for(size_t i = 1; i < weights.size(); i ++)
	{
		weights[i] = weights[i - 1] + _Particles[i]._weight;
	}
	
	// Resampling
	std::vector<ParticleFilter::particle_t> tmp = _Particles;
	
	// (a)一様分布から乱数.
	ParticleFilter::weight_t u_0 = _Uniform(_RandEng) / _Particles.size();
	
	for(size_t i = 0; i < _Particles.size(); i ++)
	{
		ParticleFilter::weight_t u = u_0 + static_cast<float>(i)/_Particles.size();
		
		// (b)重み条件を満足するパーティクルを探す.
		size_t n = 0;
		while(weights[n++] < u);
		if(n > _Particles.size() - 1)
			n = _Particles.size() - 1;
			
		// (c)パーティクルを替える.
		_Particles[i] = tmp[n];
		_Particles[i]._weight = 1.0f / _Np; // was set to 1.0f.
	}
}

/**
 * 
 */
void ParticleFilter_Color::Predict()
{
	const float var = 5.0f;
	
	for(size_t i = 0; i < _Particles.size(); i ++)
	{
		float dx = _Gaussian(_RandEng) * var;
		float dy = _Gaussian(_RandEng) * var;
		
		_Particles[i]._x += static_cast<int>(dx);
		_Particles[i]._y += static_cast<int>(dy);
	}
}

/**
 * 
 */
void ParticleFilter_Color::Weight(const image_t& frame)
{
	float CumuWeight = 0.0f;
	_MinWeight = 1.0;
	_MaxWeight = 0.0;
	_ZeroWeightParticles = 0;
	
	for(size_t i = 0; i < _Particles.size(); i ++)
	{
		_Particles[i]._weight = Likelihood(frame, _Particles[i]);
		CumuWeight += _Particles[i]._weight;
		
		if(_Particles[i]._weight == 0.0)
			_ZeroWeightParticles ++;
	}
	
	for(size_t i = 0; i < _Particles.size(); i ++)
	{
		_Particles[i]._weight = _Particles[i]._weight / CumuWeight;
		
		if(_Particles[i]._weight > _MaxWeight)
			_MaxWeight = _Particles[i]._weight;
		if(_Particles[i]._weight < _MinWeight)
			_MinWeight = _Particles[i]._weight;
	}
	
	_ZeroWeightPercent = static_cast<float>(_ZeroWeightParticles) / static_cast<float>(_Np);
	
	LOG4CPLUS_INFO("ParticleFilter_Color::Weight(): max weight = %f, min weight = %f, zero weight particles = %d, zero percent = %f",
				   _MaxWeight, _MinWeight, _ZeroWeightParticles, _ZeroWeightPercent);
}

/**
 * 
 */
void ParticleFilter_Color::Measure()
{
	double x = 0.0f;
	double y = 0.0f;
//	double weight = 0.0f;
	
	for(size_t i = 0; i < _Particles.size(); i ++)
	{
		x += _Particles[i]._x * _Particles[i]._weight;
		y += _Particles[i]._y * _Particles[i]._weight;
//		weight += _Particles(i)._weight;
	}
	
	_Pos = boost::make_tuple(static_cast<int>(x/*/weight*/), static_cast<int>(y/*/weight*/));
}

/*******************************************************************************
 * 
 * Paricle filter for Color, 2.
 * 
 *******************************************************************************/
 /**
  * 
  */
 ParticleFilter_Color_2::ParticleFilter_Color_2()
	: ParticleFilter_Color(),
	  _ColorHistThreshold(0.5)
 {
	 
 }
 
 /**
  * 
  */
ParticleFilter_Color_2::~ParticleFilter_Color_2()
{
	
}

/**
 * 
 */
void ParticleFilter_Color_2::Init(const ColorHistogram_Divided& hist, boost::tuple<int, int> initpos, boost::tuple<int, int> size)
{
	_RefHist_2 = hist;
	_Size = size;
	_Particles.resize(_Np);
	
	for(std::vector<particle_t>::iterator i = _Particles.begin(); i != _Particles.end(); i ++)
	{
		i->_x = initpos.get<0>();
		i->_y = initpos.get<1>();
		i->_weight = 1.0f;
	}
}

/**
 * 
 */
ParticleFilter::weight_t ParticleFilter_Color_2::Likelihood(const image_t& frame, const ParticleFilter::particle_t& p)
{
	int w = _Size.get<0>();
	int h = _Size.get<1>();
	int x = p._x;
	int y = p._y;
	
	if(x < 0 || y < 0 || x > frame.size().width || y > frame.size().height)
		return 0.0;
	
	if(x + w > frame.size().width)
		w = frame.size().width - x;
	if(y + h > frame.size().height)
		h = frame.size().height - y;
	
	cv::Rect roirect(x, y, w, h);
	cv::Mat ROI = frame(roirect);
	ColorHistogram_Divided hist(ROI);
	
	ColorHistogramComp_Divided cmp;
	ParticleFilter::weight_t overlap = cmp.Compare(_RefHist_2, hist);
	
	return 1.0 - overlap;
}

/**
 * 
 */
void ParticleFilter_Color_2::CorrectFilter(const image_t& frame, const std::vector<cv::Rect>& roilist)
{
	std::vector<roi_candidate> validROI;
	
	/* look at each roi and find the best one. */
	for (std::vector<cv::Rect>::const_iterator i = roilist.begin(); i != roilist.end(); i ++)
	{
		/* check distance condition. */
		int xi = i->x;
		int yi = i->y;
		
		int x = _Pos.get<0>();
		int y = _Pos.get<1>();
		int w = _Size.get<0>();
		int h = _Size.get<1>();
		
		int dx = xi - x;
		int dy = yi - y;
		
		/* the bounding box is not intersected, give up this candidate. */
		if (abs(dx) >= w || abs(dy) >= h)
			continue;
		
		/* check color histogram. */
		try
		{
			cv::Mat ROI = frame(const_cast<cv::Rect&>(*i));
			ColorHistogram_Divided hist(ROI);
			ColorHistogramComp_Divided cmp;
			ParticleFilter::weight_t d = cmp.Compare(_RefHist_2, hist);
			ParticleFilter::weight_t s = 1 - d*d;
			
			if (s < _ColorHistThreshold)
			continue;
		
			ParticleFilter::weight_t s_dist = 1 - sqrt(dx*dx + dy*dy) / sqrt(w*w + h*h);
			ParticleFilter::weight_t s_color = s;
			ParticleFilter::weight_t s_overall = s_dist * 0.75 + s_color * 0.25;
		
			roi_candidate rc(*i, s_overall);
			validROI.push_back(rc);
		}
		catch(cv::Exception& e)
		{
			continue;
		}
	}
	
	if (validROI.size() == 0)
	{
		LOG4CPLUS_INFO("ParicleFilter_Color_2::CorrectFilter: no valid candidate.");
		return;
	}
	
	/* select the best roi. */
	roi_candidate bestROI = *std::max_element(validROI.begin(), validROI.end(), 
											 boost::bind(&roi_candidate::_w, _1) < bind(&roi_candidate::_w, _2));
	
	/* correct the filter. */
	cv::Mat bestROIMat = frame(bestROI._rect);
	ColorHistogram_Divided bestHist(bestROIMat);
	Init(bestHist, 
		 boost::make_tuple(bestROI._rect.x, bestROI._rect.y), 
		 boost::make_tuple(bestROI._rect.width, bestROI._rect.height));
	
	/* reset resample count. */
	ResetResampleCounter();
	
	LOG4CPLUS_INFO("ParicleFilter_Color_2::CorrectFilter: corrected.");
}

/*******************************************************************************
 * 
 * Paricle filter for Color, 2.
 * 
 *******************************************************************************/
/**
 * 
 */
KalmanFilter::KalmanFilter()
	: _kf(4, 2, 0, CV_32F),
	  _dt(1.0f)
{
	cv::setIdentity(_kf.measurementMatrix);
	cv::setIdentity(_kf.processNoiseCov, cv::Scalar::all(1e-5));
	cv::setIdentity(_kf.measurementNoiseCov, cv::Scalar::all(1e-5));
	cv::setIdentity(_kf.errorCovPost, cv::Scalar::all(1));
	
	_kf.transitionMatrix = (cv::Mat_<float>(4, 4) << 1, 0, _dt, 0,
													0, 1, 0, _dt,
													0, 0, 1, 0,
													0, 0, 0, 1);
}

/**
 * 
 */
KalmanFilter::~KalmanFilter()
{
	
}

/**
 * 
 */
void KalmanFilter::Init(boost::tuple<int, int> initpos, boost::tuple<int, int> size)
{
	_Pos = initpos;
	_Size = size;
	_kf.statePost = (cv::Mat_<float>(4, 1) << _Pos.get<0>(),
											  _Pos.get<1>(),
											  0.0f,
											  0.0f);
}

/**
 * 
 */
void KalmanFilter::Run(const image_t& frame)
{
	frame.empty();
	cv::Mat pred = _kf.predict();
	_Pos.get<0>() = pred.at<float>(0);
	_Pos.get<1>() = pred.at<float>(1);
}
 
