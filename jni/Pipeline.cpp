#include <boost/timer.hpp>
#include <vector>
#include <algorithm>

#include "Pipeline.h"
#include "Utils.h"
#include "Logger.h"

/******************************************************************************
 *
 * Pipeline
 *
 ******************************************************************************/
Pipeline* Pipeline::_Inst = 0;

/**
 * 
 */
Pipeline::Pipeline()
{
	_Detector.Init();
	Reset();
}

/**
 * 
 */
Pipeline::~Pipeline()
{

}

/**
 * 
 */
void Pipeline::Reset()
{
	_Fps = 0.0f;
	_DetectorActCount = 0;
	_RealtimeDisplay = true;
	_FrmCnt = 0;
	_DetectionPosition.clear();
}

/**
 *
 */
void Pipeline::ProcessFrame(cv::Mat& matFrame, int width, int height)
{
	double time = 0.0f;
	boost::timer t;
		
	/* Clear object list. */
	_ObjRect.clear();
	_ObjRectFiltered.clear();
		
	if(_TM.GetTrackerNumber() == 0)
	{
		_Detector.Run(matFrame, _ObjRect, _ObjRectFiltered);
		_TM.Run(matFrame, _ObjRect, _FrmCnt);
			
		_DetectorActCount ++;
		_DetectionPosition.push_back(_FrmCnt);
	}
	else
	{
		if (_TM.NeedCorrection() == true)
		{
			boost::timer td;
			_Detector.Run(matFrame, _ObjRect, _ObjRectFiltered);
			time = td.elapsed();
				
			_DetectorActCount ++;
			_DetectionPosition.push_back(_FrmCnt);

			_TM.Update(matFrame, _ObjRect);
		}
				
		_TM.Run(matFrame, _ObjRect, _FrmCnt);
		LOG4CPLUS_INFO("Pipeline::ProcessFrame(): tracking frame %d.\n", _FrmCnt);
	}
		
	_FrmCnt ++;
		
	double elapsedTime = t.elapsed();
	_Fps = 1 / elapsedTime;
}

Pipeline* Pipeline::getInstance()
{
	if (!Pipeline::_Inst)
		Pipeline::_Inst = new Pipeline();
	return Pipeline::_Inst;
}
