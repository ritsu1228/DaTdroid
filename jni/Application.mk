APP_STL := gnustl_static
APP_CPPFLAGS := -frtti -fexceptions -mfpu=vfpv3-d16 -ftree-vectorize -mfloat-abi=softfp -ffast-math
APP_ABI := armeabi-v7a
