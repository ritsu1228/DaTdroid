LOCAL_PATH := $(call my-dir)
#OPENCV_PATH := C:/OpenCV-2.3.1-android-bin/OpenCV-2.3.1
#OPENCV_MK_PATH := $(OPENCV_PATH)/share/OpenCV/OpenCV.mk
OPENCV_PATH := $(LOCAL_PATH)/../opencv-2.2
OPENCV_MK_PATH := $(OPENCV_PATH)/android-opencv.mk
BOOST_PATH := C:/boost_1_47_0

include $(CLEAR_VARS)
include $(OPENCV_MK_PATH)

LOCAL_MODULE := datdroid

LOCAL_SRC_FILES := \
	Utils.cpp \
	Filters.cpp \
	Trackers.cpp \
	ObjDetect.cpp \
	Pipeline.cpp \
	DaTdroid.cpp

LOCAL_LDLIBS +=  -llog -ldl $(OPENCV_LIBS)
LOCAL_C_INCLUDES += $(BOOST_PATH) $(OPENCV_INCLUDES)

ifeq ($(TARGET_ARCH_ABI),armeabi-v7a)
LOCAL_ARM_NEON := true
endif

include $(BUILD_SHARED_LIBRARY)
