#ifndef __UTILS_H__
#define __UTILS_H__
/*******************************************************************************
 * 
 * 
 * 
 *******************************************************************************/
#include <opencv/cv.h>

/*******************************************************************************
 * 
 * 
 * 
 *******************************************************************************/
class ColorHistogram
{
public:
	ColorHistogram() {}

	ColorHistogram(const cv::Mat& img)
	{
		Set(img);
	}
	
	ColorHistogram(const ColorHistogram& rhs)
		: _Hist(rhs._Hist)
	{
	}
	
	ColorHistogram& operator=(const ColorHistogram rhs)
	{
		_Hist = rhs._Hist;
		return *this;
	}
	
	~ColorHistogram() {}
	
public:
	void Set(const cv::Mat& img)
	{
		cv::Mat mask;
		int channels[] = {0, 1, 2};
		float rrange[] = {0, 255};
		float grange[] = {0, 255};
		float brange[] = {0, 255};
		const float* ranges[] = {rrange, grange, brange};
		int HistSize[] = {64, 64, 64};
		
		cv::calcHist(&img, 1, channels, mask, _Hist, 3, HistSize, ranges);
	}
	const cv::MatND& GetHistogram() const { return _Hist; }
	cv::MatND& GetHistogram() { return _Hist; }

private:
	cv::MatND _Hist;
};

/*******************************************************************************
 * 
 * 
 * 
 *******************************************************************************/
 class ColorHistogram_Divided
 {
public:
	friend class ColorHistogramComp_Divided;
	
	ColorHistogram_Divided(float ratio = 0.5)
		: _ratio(ratio)
	{}
	
	ColorHistogram_Divided(const cv::Mat& img, float ratio = 0.5)
		: _ratio(ratio)
	{
		Set(img);
	}
	
	~ColorHistogram_Divided() {}
	
	ColorHistogram_Divided& operator=(const ColorHistogram_Divided& rhs)
	{
		_Hist_part_1 = rhs._Hist_part_1;
		_Hist_part_2 = rhs._Hist_part_2;
		return *this;
	}
	
public:
	void Set(const cv::Mat& img)
	{
		/* Initial parameters. */
		cv::MatND	mask;
		int channels[] = {0, 1, 2};
		float rrange[] = {0, 255};
		float grange[] = {0, 255};
		float brange[] = {0, 255};
		const float* ranges[] = {rrange, grange, brange};
		int HistSize[] = {64, 64, 64};
		
		/* sub images. */
		cv::Rect	sub_1(0, 0, img.size().width, img.size().height * _ratio);
		cv::Rect	sub_2(0, img.size().height * _ratio, img.size().width, img.size().height * _ratio);
		
		cv::Mat	subimg_1 = img(sub_1);
		cv::Mat	subimg_2 = img(sub_2);
		
		/* calc histograms. */
		cv::calcHist(&subimg_1, 1, channels, mask, _Hist_part_1, 3, HistSize, ranges);
		cv::calcHist(&subimg_2, 1, channels, mask, _Hist_part_2, 3, HistSize, ranges);
	}

private:
	cv::MatND	_Hist_part_1;
	cv::MatND	_Hist_part_2;
	float		_ratio;
 };
 
 /*******************************************************************************
 * 
 * 
 * 
 *******************************************************************************/
 class ColorHistogramComp_Divided
 {
public:
	explicit ColorHistogramComp_Divided(float w_1 = 0.5, float w_2 = 0.5) : _w_1(w_1), _w_2(w_2) {}
	ColorHistogramComp_Divided(const ColorHistogramComp_Divided& rhs) {_w_1 = rhs._w_1; _w_2 = rhs._w_2;}
	~ColorHistogramComp_Divided() {}
	
public:
	double Compare(const ColorHistogram_Divided& _1, const ColorHistogram_Divided& _2)
	{
		double s_1 = cv::compareHist(_1._Hist_part_1, _2._Hist_part_1, CV_COMP_BHATTACHARYYA);
		double s_2 = cv::compareHist(_1._Hist_part_2, _2._Hist_part_2, CV_COMP_BHATTACHARYYA);
		double s = s_1 * _w_1 + s_2 * _w_2;
		return s;
	}
	
private:
	float _w_1;
	float _w_2;
 };

/*******************************************************************************
 * 
 * 
 * 
 *******************************************************************************/
inline double operator-(const ColorHistogram& h1, const ColorHistogram& h2)
{
	return cv::compareHist(h1.GetHistogram(), h2.GetHistogram(), CV_COMP_BHATTACHARYYA);
}

#endif