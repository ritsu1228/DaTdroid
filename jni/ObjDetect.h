#ifndef OBJ_DETECT_H
#define OBJ_DETECT_H

#include "common.h"

/*******************************************************************************
 *
 *
 *
 *******************************************************************************/
class ObjectDetector
{
public:
	ObjectDetector();
	virtual ~ObjectDetector();

public:
	void Init();
	void Run(cv::Mat& frame, std::vector<cv::Rect>& found, std::vector<cv::Rect>& filtered);

private:
	cv::HOGDescriptor _Hog;
};

#endif
