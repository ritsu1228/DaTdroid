#include "Trackers.h"
#include "Logger.h"

/*******************************************************************************
 * 
 * Tracker Manager
 * 
 *******************************************************************************/
/**
 * 
 */
TrackerManager::TrackerManager()
	: _TrackersNeedCorrection(false)
{
}
 
/**
 * 
 */
TrackerManager::~TrackerManager()
{
	Cleanup();
}

/**
 * 
 */
void TrackerManager::Init()
{
	Cleanup();
}

/**
 * 
 */
void TrackerManager::Cleanup()
{
	for(TrackerManager::tracker_iter_t i = _TrackerList.begin(); i != _TrackerList.end(); i ++)
	{
		if(*i)
		{
			delete *i;
		}
	}
	
	_TrackerList.clear();
}

/**
 * 
 */
void TrackerManager::Run(image_t& frame, const std::vector<cv::Rect>& roilist, boost::int32_t frmcnt)
{
	_CurrentFrameNum = frmcnt;
	
	if(_TrackerList.empty())
	{
		/* when no trackers exist and we have found something. */
		if (!roilist.empty())
		{
			Tracker_Color* tk = new Tracker_Color();
			ColorHistogram_Divided ref(frame(roilist[0]));
			tk->Init(ref, boost::make_tuple(roilist[0].x, roilist[0].y), boost::make_tuple(roilist[0].width, roilist[0].height));
			_TrackerList.push_back(tk);

			LOG4CPLUS_INFO("TrackerManager::Run(): new tracker added at frame %d.\n", frmcnt);
		}
	}
	else
	{
		_TrackerList[0]->Run(frame);
		_TrackerList[0]->DrawParticles(frame);
		
		/* we check which track(s) needs update here. */
		for (size_t i = 0; i < _TrackerList.size(); i ++)
		{
			if (_TrackerList[i]->NeedCorrection() == true)
			{
				_TrackersNeedCorrection = true;
				_TrackerIdxForCorr.push_back(i);
			}
		}
	}
}

/**
 *
 */
void TrackerManager::Update(image_t& frame, const std::vector<cv::Rect>& roilist)
{
	if (_TrackersNeedCorrection == true)
	{
		_TrackersNeedCorrection = false;
		for (size_t i = 0; i < _TrackerList.size(); i ++)
		{
			_TrackerList[i]->CorrectTracker(frame, roilist);
		}
	}
}

/*******************************************************************************
 * 
 * Tracker color.
 * 
 *******************************************************************************/
/**
 * 
 */
Tracker_Color::Tracker_Color()
	: TrackerBase(),
	  _PF()
{
	_ResamplingThreshold = 20;
	_DetectEveryFrame = false;
	_UseRateThreshold = false;
	_ResamplingRateThreshold = 0;
}

/**
 * 
 */
Tracker_Color::~Tracker_Color()
{
	
}

/**
 * 
 */
void Tracker_Color::Init(const ColorHistogram_Divided& hist, boost::tuple<int, int> initpos, boost::tuple<int, int> size)
{
	_PF.Init(hist, initpos, size);
}

/**
 * 
 */
void Tracker_Color::Run(const image_t& frame)
{
	_PF.Run(frame);
}

/**
 * 
 */
void Tracker_Color::DrawParticles(image_t& frame)
{
	_PF.DrawParticles(frame);
}

/**
 * 
 */
bool Tracker_Color::NeedCorrection()
{
	if (_DetectEveryFrame == true)
		return true;
	else
	{
		if (_UseRateThreshold == true)
		{
			if (_PF.GetResampleCounter() > _ResamplingThreshold)
				return true;
			return _PF.GetResampleRate() < _ResamplingRateThreshold;
		}
		else
			return _PF.GetResampleCounter() > _ResamplingThreshold;
	}
}

/*******************************************************************************
 * 
 * Tracker Kalman.
 * 
 *******************************************************************************/
/**
 * 
 */
Tracker_Kalman::Tracker_Kalman()
{
	
}

/**
 * 
 */
Tracker_Kalman::~Tracker_Kalman()
{
	
}
