package ritsu.datdroid;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public abstract class DaTdroidViewBase extends SurfaceView implements SurfaceHolder.Callback, Runnable {

	private Camera mCamera;
	private SurfaceHolder mHolder;
	private int mFrameWidth;
	private int mFrameHeight;
	private byte[] mFrame;
	private boolean mThreadRun;
	
	/**
	 * 
	 * @param context
	 */
	public DaTdroidViewBase(Context context) {
		super(context);
		mHolder = getHolder();
		mHolder.addCallback(this);
	}

	/**
	 * 
	 * @return
	 */
	public int getFrameWidth() {
		return mFrameWidth;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getFrameHeight() {
		return mFrameHeight;
	}
	
	/**
	 * 
	 */
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if (mCamera != null) {
			Camera.Parameters params = mCamera.getParameters();
			List<Camera.Size> sizes = params.getSupportedPreviewSizes();
			mFrameWidth = width;
			mFrameHeight = height;
			
			{
				double minDiff = Double.MAX_VALUE;
				for (Camera.Size size : sizes) {
					if (Math.abs(size.height - height) < minDiff) {
						mFrameWidth = size.width;
						mFrameHeight = size.height;
						minDiff = Math.abs(size.height - height);
					}
				}
			}
			
			mFrameWidth = 320;
			mFrameHeight = 240;
			
			params.setPreviewSize(getFrameWidth(), getFrameHeight());
			mCamera.setParameters(params);
			
			try {
				mCamera.setPreviewDisplay(null);
			} catch (IOException e) {
				
			}
			mCamera.startPreview();
		}
	}
	
	/**
	 * 
	 */
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		mCamera = Camera.open();
		mCamera.setPreviewCallback(new PreviewCallback() {
			public void onPreviewFrame(byte[] data, Camera camera) {
				synchronized(DaTdroidViewBase.this) {
					mFrame = data;
					DaTdroidViewBase.this.notify();
				}
			}
		});
		(new Thread(this)).start();
	}
	
	/**
	 * 
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		mThreadRun = false;
		if (mCamera != null) {
			synchronized(this) {
				mCamera.stopPreview();
				mCamera.setPreviewCallback(null);
				mCamera.release();
				mCamera = null;
			}
		}
	}
	
	/**
	 * 
	 * @param data
	 * @return
	 */
	protected abstract Bitmap processFrame(byte[] data);
	
	/**
	 * 
	 */
	@Override
	public void run() {
		mThreadRun = true;
		while (mThreadRun) {
			Bitmap bmp = null;
			
			synchronized(this) {
				try {
					this.wait();
					bmp = processFrame(mFrame);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			if (bmp != null) {
				Canvas canvas = mHolder.lockCanvas();
				if (canvas != null) {
					canvas.drawBitmap(bmp, 
									  (canvas.getWidth() - getFrameWidth())/2,
									  (canvas.getHeight() - getFrameHeight())/2,
									  null);
					mHolder.unlockCanvasAndPost(canvas);
				}
				bmp.recycle();
			}
		}
	}
}
